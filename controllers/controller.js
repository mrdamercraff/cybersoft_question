let renderRadionButton = (data) => {
  return `<div class="form-check">
        <label class="form-check-label">
        <input type="radio" class="form-check-input" name="singleChoice" id="" value="${data.exact}" >
        ${data.content}
      </label>
    </div>`;
};
let renderSingleChoice = (question) => {
  let contentAsnwer = "";
  question.answers.forEach((item) => {
    let radioHTML = renderRadionButton(item);
    contentAsnwer += radioHTML;
  });
  return `<p>${question.content}</p>
  <div>
  ${contentAsnwer}</div>`;
};
const renderFillToInput = (question) => {
  return `<div class="form-group">
    <label for="">${question.content}</label>
    <input type="text" data-no-answer="${question.answers[0].content}"
      class="form-control" id="fillInput"  >
    
  </div>`;
};
export const checkFillInput = () => {
  let inputEl = document.getElementById("fillInput");
  if (inputEl.value == inputEl.dataset.noAnswer) {
    return true;
  } else {
    return false;
  }
};
export const checkSingleChoice = () => {
  let inputElement = document.querySelector(
    'input[name="singleChoice"]:checked'
  );

  if (inputElement == null || inputElement.value == "false") {
    return false;
  }
  if (inputElement.value == "true") {
    return true;
  }
};
export const renderQuestion = (questions) => {
  if (questions.questionType == 1) {
    // render singgleChoise
    document.getElementById("contentQuiz").innerHTML =
      renderSingleChoice(questions);
  } else {
    document.getElementById("contentQuiz").innerHTML =
      renderFillToInput(questions);
  }
};

export const showResult = (list) => {
  document.getElementById("endQuiz").classList.remove("d-none");
  document.getElementById("startQuiz").style.display = "none";
  let count = 0;
  list.forEach((item) => {
    if (item.isCorrect) {
      count++;
    }
  });
  document.getElementById("correct").innerHTML = count;
  document.getElementById("incorrect").innerHTML = list.length - count;
  document.getElementById("score").innerHTML = `${count}/${list.length}`;
};
