import { dataQuestion as dataRaw } from "../data/questions.js";
import {
  checkFillInput,
  checkSingleChoice,
  renderQuestion,
  showResult,
} from "./controller.js";
// render Lần đầu

let currentQuestionIndex = 0;
let dataQuestion = dataRaw.map((item) => {
  return { ...item, isCorrect: null };
});
console.table(dataQuestion);
document.getElementById("currentStep").innerHTML = `${
  currentQuestionIndex + 1
}/${dataQuestion.length}`;

renderQuestion(dataQuestion[currentQuestionIndex]);
function nextQuestion() {
  if (dataQuestion[currentQuestionIndex].questionType == 1) {
    dataQuestion[currentQuestionIndex].isCorrect = checkSingleChoice();
    console.table(dataQuestion);
  } else {
    dataQuestion[currentQuestionIndex].isCorrect = checkFillInput();
    console.table(dataQuestion);
  }
  currentQuestionIndex++;
  if (currentQuestionIndex == dataQuestion.length) {
    showResult(dataQuestion);
    return;
  }

  document.getElementById("currentStep").innerHTML = `${
    currentQuestionIndex + 1
  }/${dataQuestion.length}`;
  renderQuestion(dataQuestion[currentQuestionIndex]);
}
window.nextQuestion = nextQuestion;
